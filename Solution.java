import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'minimumBribes' function below.
     *
     * The function accepts INTEGER_ARRAY q as parameter.
     */

    public static void minimumBribes(List<Integer> q) {
        // Write your code here

        int numberOfBribes = 0;
        boolean flag = false;
        int size = q.size();

        for (int i = size; i > 1; i--) {
            if (q.get(i-1) != i) {
                if ((i -2) >= 0 && q.get(i - 2) == i ) {
                    int swap1 = q.get(i -1);
                    int swap2 = q.get(i -2);
                    q.set(i - 1, swap2);
                    q.set(i - 2, swap1);
                    numberOfBribes++;

                } else if ((i - 3) >= 0 && q.get(i -3) == i) {
                    int swap1 = q.get(i -1);
                    int swap2 = q.get(i -2) ;
                    int swap3 = q.get(i - 3) ;
                    q.set(i-1, swap3);
                    q.set(i -2, swap1);
                    q.set(i -3, swap2);
                    numberOfBribes += 2;
                } else {
                    flag = true;
                    break;
                }

            }

        }
        System.out.println(flag?"Too chaotic":numberOfBribes);



    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                int n = Integer.parseInt(bufferedReader.readLine().trim());

                List<Integer> q = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList());

                Result.minimumBribes(q);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
    }
}
