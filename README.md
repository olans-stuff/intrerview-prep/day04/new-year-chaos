
# New Year Chaos

It is New Years Day and people are in line for the wonderland rollercoaster ride. 

Each person wears a sticker indicating their initial position in the queue from 1 to n

Any person can bribe at most two others.
Determine the minimum number of bribes that took place to get to a given queue order.

Print the number of bribes, or, if anyone has bribed more than two people, print `Too chaotic`.

# Example
q = `[1,2,3,5,4,6,7,8]`
if person 5 bribes 4, the queue will look like this `[1,2,3,5,4,6,7,8]`
Only 1 bribe is required. Print `1` 
q = [4,1,2,3]

Person 4 had to bribe 3 people to get to the current position. Print `too chaotic`.

### Function Description

Complete the function minimumBribes in the editor below.

minimumBribes has the following parameter(s):

- int q[n]: the positions of the people after all bribes
###   Returns

No value is returned. Print the minimum number of bribes necessary or Too chaotic if someone has bribed more than 2 people.

### Input Format

The first line contains an integer t, the number of test cases.

Each of the next t pairs of lines are as follows:
- The first line contains an integer t, the number of people in the queue
- The second line has n space-separated integers describing the final state of the queue.

## Constraints 

- 1 <= t ,= 10
- 1 <= n <= 10 ^5

## Subtasks
For 60% score 1 <= n <= 10^3

For 100% score 1<= n <= 10^5

### Sample input
~~~
STDIN       Function
-----       --------
2           t = 2
5           n = 5
2 1 5 3 4   q = [2, 1, 5, 3, 4]
5           n = 5
2 5 1 3 4   q = [2, 5, 1, 3, 4]
~~~

### Sample Output
~~~
3
Too chaotic
~~~
# Explanation

### Test Case 1

The initial state:
![img.png](img.png)

--------------------------------------------
couldnt get so used this video
--------------------------------------------
 https://www.youtube.com/watch?v=RGv_K72p86M


